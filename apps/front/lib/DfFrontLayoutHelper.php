<?php

class dfFrontLayoutHelper extends dmFrontLayoutHelper {

    public function renderJquery() {
        
        $user = $this->getService('user');
        
        if ($user->can('tool_bar_front, widget_edit_fast')) {
            return '<script src="/js/jquery-1.7.1.min.js"></script>';
        } else {
            return '<script src="/js/jquery-1.11.0.min.js"></script>';
        }
    }

}
