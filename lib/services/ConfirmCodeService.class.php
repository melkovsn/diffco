<?php

/**
 * AccountTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class ConfirmCodeService {

    protected static $instance;

    public static function getInstance() {

        if (self::$instance === NULL) {

            self::$instance = new self();
        }

        return self::$instance;
    }
    
    public function sendConfirmCode($phone){
        
        $confirmCode = 7777;//rand(1000, 9999);
        
        sfContext::getInstance()->getUser()->setAttribute('confirm_code', $confirmCode);
        sfContext::getInstance()->getUser()->setAttribute('confirm_code_time', time());
        
        return $this->sendConfirmSms($phone, $confirmCode);
    }

    public function checkConfirmCode($confirmCode, $timeAlive=300){
        
        $savedCode = sfContext::getInstance()->getUser()->getAttribute('confirm_code');
        
        $savedTime = sfContext::getInstance()->getUser()->getAttribute('confirm_code_time');
        
        $isCorrect = ($savedCode == $confirmCode) && ((time()-$savedTime < $timeAlive)) ;
        
        if ($isCorrect){
            sfContext::getInstance()->getUser()->getAttributeHolder()->remove('confirm_code');
            sfContext::getInstance()->getUser()->getAttributeHolder()->remove('confirm_code_time');
        }
        
        return $isCorrect;
    }
    
    private function sendConfirmSms($phone, $confirmCode){
        
        /// Заглушка
        return true;
    }
}
