<?php

/**
 * Счёт components
 * 
 * No redirection nor database manipulation ( insert, update, delete ) here
 */
class accountComponents extends myFrontModuleComponents {

    public function executeAccountListClient(dmWebRequest $request) {
        
        $this->client = DfAuthUser::getInstance()->getAuthUser();

        $this->currencyAccounts = $this->client->getCurrencyAccounts();
        $this->depoAccounts = $this->client->getDepoAccounts();
    }

}
