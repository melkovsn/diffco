$(document).ready(function () {

    // валидация полей дат
    $.validator.addMethod(
            "dateFormat",
            function (value, element) {
                // put your own logic here, this is just a (crappy) example
                return value.match(/^\d\d?\.\d\d?\.\d\d\d\d$/);
            },
            "Введите дату в формате dd.mm.yyyy  "
            );
    $('#personal_data').validate({
        rules: {
            passport_date: {
                date: false,
                dateFormat: true
            },
            birth_date: {
                date: false,
                dateFormat: true
            }
        }
    });

    // отправка формы    
    $('#personal_data').submit(function (event) {

        event.preventDefault();

        if ($('#personal_data').valid()) {
            $("#personal_data .post_error").hide();
            $("#personal_data .post_success").hide();

            var form = document.forms.personal_data;

            var formData = new FormData(form);
            var xhr = new XMLHttpRequest();

            xhr.open("POST", "/+/client/sendPersonalData");

            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        data = xhr.responseText;

                        if (data == "success") {
                            $("#personal_data .post_error").hide();
                            $("#personal_data .post_success").show();
                        } else {
                            $("#personal_data .post_error").show();
                            $("#personal_data .post_success").hide();
                        }
                    }
                }
            };

            xhr.send(formData);
        }

    });

    var timer;
    var $confirmCode = $('#confirm_code');
    var $confirmCodeTime = $('#confirm_code_time');
    var $codeTrueModal = $('#code_true_modal');

    $('.save_contact, .more_code').on('click', function () {

        clearInterval(timer);
        $confirmCodeTime.val('');
        $confirmCode.val('');
        $confirmCode.removeClass('error');

        $.post("/+/user/sendConfirmCode", function (data) {
            data = jQuery.parseJSON(data);
            if (data['status'] == 'success') {
                $codeTrueModal.modal();

                countdown(data['time_alive']);
            }
        });

        return false;
    });

    function countdown(seconds) {

        timer = setInterval(function () {

            var date = new Date(1970, 0, 1);

            date.setSeconds(seconds--);

            if (seconds < 0) {
                clearInterval(timer)
            }

            var timeAlive = moment(date);
            $confirmCodeTime.val(timeAlive.format('mm:ss'));

        }, 1000);
    }

    $('.save_contact').on('click', function () {
        $codeTrueModal.attr('data_field', ($(this).attr('data_field')));
    });

    $('#confirm_code_form').submit(function (event) {

        event.preventDefault();

        var fieldId = $codeTrueModal.attr('data_field');

        var $field = $('#' + fieldId);

        var data = {'field': fieldId, 'value': $field.val(), 'confirm_code': $confirmCode.val()};

        $.post("/+/client/saveField", data, function (data) {
            data = jQuery.parseJSON(data);

            if (data['status'] == 'success') {
                $field.removeClass('error');

                var p = $field.parent().next('.contact_controls');

                $(p).find('.save_contact').hide();
                $(p).find('.cancel_contact').hide();
                $(p).find('.edit_field').css('display', 'inline-block');
                $field.attr('disabled', 'disabled');

                $codeTrueModal.modal('hide');
            } else if (data['status'] == 'code_error') {
                if (!$confirmCode.hasClass('error')) {
                    $confirmCode.addClass('error');
                }
            } else {
                $field.addClass('error');
                $codeTrueModal.modal('hide');
            }

        });

    });

    function saveField(fieldId) {
        var $field = $('#' + fieldId);

        var data = {'field': fieldId, 'value': $field.val()};

        $.post("/+/client/saveField", data, function (data) {
            data = jQuery.parseJSON(data);

            if (data['status'] == 'success') {
                $field.removeClass('error');

                var p = $field.parent().next('.contact_controls');

                $(p).find('.save_contact').hide();
                $(p).find('.cancel_contact').hide();
                $(p).find('.edit_field').css('display', 'inline-block');
                $field.attr('disabled', 'disabled');
            } else {
                if (!$field.hasClass('error')) {
                    $field.addClass('error');
                }
            }
        });
    }

    $('.cancel_contact').on('click', function () {
        var p = $(this).closest('.contact_controls');
        $(this).hide();
        $(p).find('.save_contact').hide();
        $(p).find('.edit_field').css('display', 'inline-block');

        var input = $(this).closest('.line').find('input.form-control');
        $(input).attr('disabled', 'disabled');
        $(input).val($(input).attr('default-value'));

        return false;
    });

    $('.bank_block_edit_link').on('click', function () {

        var data = {'bankDetailsId': $(this).attr('data_bank-details-id')};

        $.post("/+/client/renderBankDetailsForm", data, function (renderHTML) {
            $('#bank_block_edit_form_fields').html(renderHTML);

            $('#beneficiary_bank_country_id, #currency_id').selectpicker();

            checkCountry();

            $('#beneficiary_bank_country_id').change(function () {
                checkCountry();
            });

            $('#bank_block_edit_form').validate();

            $('#bank_block_send_to_manager_link').on('click', sendMailRequestToManager);

            $('.cancel_edit_bank').on('click', function () {
                $('.bank_block_edit').fadeOut();
                $('.bank_block').fadeIn();
                return false;
            });
        });

        return false;
    });

    function sendMailRequestToManager(event) {
        event.preventDefault();

        $("#bank_block_edit_form .post_error").hide();
        $("#bank_block_edit_form .post_success").hide();

        $("#bank_block_edit_form .form-control").removeClass('error');

        var form = document.forms.bank_block_edit_form;

        var formData = new FormData(form);
        var xhr = new XMLHttpRequest();

        xhr.open("POST", "/+/client/sendBankDetailsRequestToManager");

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    data = xhr.responseText;

                    if (data == "success") {
                        $("#bank_block_edit_form .post_error").hide();
                        $("#bank_block_edit_form .post_success").show();
                    } else {
                        $("#bank_block_edit_form .post_error").show();
                        $("#bank_block_edit_form .post_success").hide();
                    }
                }
            }
        };

        xhr.send(formData);

        return false;
    }

    function checkCountry() {
        var parent_div = $('#currency_id').parent();
        var bootstrapOptionRUR = $(parent_div).find('.bootstrap-select span:contains("RUR")').parents('li');
        var $bootstrapOptionRUR = $(bootstrapOptionRUR);

        if ($('#beneficiary_bank_country_id :selected').text() != 'Россия') {
            var currency = $('#currency_id :selected').text();

            if (currency == 'RUR') {
                $bootstrapOptionRUR.siblings().eq(1).find('a').trigger('click');
            }
            $(parent_div).find('.bootstrap-select span:contains("RUR")').parents('li').addClass('disabled');
            $(parent_div).find('option:contains("RUR")').attr('disabled', 'disabled');
            $('.no_russia_bank').show();
            $('.russia_bank').hide();

            $('.russia_bank .form-control').each(function () {
                if ($(this).hasClass('required')) {
                    $(this).removeClass('required');
                }
            });

            $('.no_russia_bank .form-control').each(function () {
                if (!$(this).hasClass('required')) {
                    $(this).addClass('required');
                }
            });

        } else {
            if ($bootstrapOptionRUR.is('.disabled')) {
                $bootstrapOptionRUR.removeClass('disabled');
                $(parent_div).find('option:contains("RUR")').removeAttr('disabled');
            }
            $('.no_russia_bank').hide();
            $('.russia_bank').show();

            $('.no_russia_bank .form-control').each(function () {
                if ($(this).hasClass('required')) {
                    $(this).removeClass('required');
                }
            });

            $('.russia_bank .form-control').each(function () {
                if (!$(this).hasClass('required')) {
                    $(this).addClass('required');
                }
            });
        }
    }

    $('#bank_block_edit_form').submit(function (event) {

        event.preventDefault();

        if ($('#bank_block_edit_form').valid()) {

            $("#bank_block_edit_form .post_error").hide();
            $("#bank_block_edit_form .post_success").hide();

            var form = document.forms.bank_block_edit_form;

            var formData = new FormData(form);
            var xhr = new XMLHttpRequest();

            xhr.open("POST", "/+/client/sendBankDetails");

            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        data = xhr.responseText;

                        if (data == "success") {
                            $("#bank_block_edit_form .post_error").hide();
                            $("#bank_block_edit_form .post_success").show();
                        } else {
                            $("#bank_block_edit_form .post_error").show();
                            $("#bank_block_edit_form .post_success").hide();
                        }
                    }
                }
            };

            xhr.send(formData);
        }
    });

    $('.bank_block_edit_link').on('click', function () {
        $('.bank_block_edit').fadeIn();
        $('.bank_block').fadeOut();
        return false;
    });

    $('.close_bank_edit').on('click', function () {
        $('.bank_block_edit').fadeOut();
        $('.bank_block').fadeIn();
        return false;
    });

});