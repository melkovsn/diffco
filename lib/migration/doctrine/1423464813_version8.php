<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version8 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createForeignKey('report', 'report_account_id_account_id', array(
             'name' => 'report_account_id_account_id',
             'local' => 'account_id',
             'foreign' => 'id',
             'foreignTable' => 'account',
             'onUpdate' => '',
             'onDelete' => 'CASCADE',
             ));
        $this->addIndex('report', 'report_account_id', array(
             'fields' => 
             array(
              0 => 'account_id',
             ),
             ));
    }

    public function down()
    {        
        $this->dropForeignKey('report', 'report_account_id_account_id');
        $this->removeIndex('report', 'report_account_id', array(
             'fields' => 
             array(
              0 => 'account_id',
             ),
             ));
    }
}