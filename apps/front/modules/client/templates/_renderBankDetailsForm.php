<div class="line">
    <label class="col-sm-3 control-label" for="name_account">Наименование счёта:</label>
    <div class="col-sm-7">
        <?php
        $field = $form['name_account'];
        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
        ?>
    </div>
    <div class="clearfix visible-sm"></div>
</div>
<div class="line">
    <label class="col-sm-3 control-label" for="recipient">Получатель:</label>
    <div class="col-sm-7">
        <?php
        $field = $form['recipient'];
        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
        ?>
    </div>
    <div class="clearfix visible-sm"></div>
</div>
<div class="line">
    <label class="col-sm-3 control-label" for="beneficiary_bank_country_id">Страна банка-получателя:</label>
    <div class="col-sm-3">
        <?php
        $field = $form['beneficiary_bank_country_id'];
        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
        ?>
    </div>
    <label class="col-sm-1 control-label" for="currency_id">Валюта:</label>
    <div class="col-sm-3">
        <?php
        $field = $form['currency_id'];
        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
        ?>
    </div>
    <div class="clearfix visible-sm"></div>
</div>

<div class="russia_bank">
    <div class="line">
        <label class="col-sm-3 control-label" for="bank_name">Наименование банка:</label>
        <div class="col-sm-7">
            <?php
            $field = $form['bank_name'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="bank_address">Адрес банка:</label>
        <div class="col-sm-7">
            <?php
            $field = $form['bank_address'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="bik">БИК:</label>
        <div class="col-sm-3">
            <?php
            $field = $form['bik'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="bank_kpp">КПП банка:</label>
        <div class="col-sm-3">
            <?php
            $field = $form['bank_kpp'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="bank_inn">ИНН банка:</label>
        <div class="col-sm-3">
            <?php
            $field = $form['bank_inn'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="correspondent_account">Кор.счет:</label>
        <div class="col-sm-7">
            <?php
            $field = $form['correspondent_account'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>                            
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="receiver_settlement_account">Р/с получателя:</label>
        <div class="col-sm-7">
            <?php
            $field = $form['receiver_settlement_account'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="receiver_inn">ИНН получателя:</label>
        <div class="col-sm-3">
            <?php
            $field = $form['receiver_inn'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="receiver_kpp">КПП получателя:</label>
        <div class="col-sm-3">
            <?php
            $field = $form['receiver_kpp'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
</div>
<div class="no_russia_bank">
    <div class="line">
        <label class="col-sm-3 control-label" for="beneficiary_bank_name">Наименование банка-получателя:</label>
        <div class="col-sm-7">
            <?php
            $field = $form['beneficiary_bank_name'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="beneficiary_bank_swift_code">Swift банка-получателя</label>
        <div class="col-sm-7">
            <?php
            $field = $form['beneficiary_bank_swift_code'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="beneficiary_bank_address">Адрес банка-получателя:</label>
        <div class="col-sm-3">
            <?php
            $field = $form['beneficiary_bank_address'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="beneficiary_bank_account">Счет в банке-получателе:</label>
        <div class="col-sm-3">
            <?php
            $field = $form['beneficiary_bank_account'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="correspondent_bank_name">Наименование банка-корреспондента:</label>
        <div class="col-sm-3">
            <?php
            $field = $form['correspondent_bank_name'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="correspondent_bank_swift_code">Swift банка-получателя:</label>
        <div class="col-sm-7">
            <?php
            $field = $form['correspondent_bank_swift_code'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="correspondent_bank_address">Адрес банка-получателя:</label>
        <div class="col-sm-7">
            <?php
            $field = $form['correspondent_bank_address'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
    <div class="line">
        <label class="col-sm-3 control-label" for="correspondent_bank_account">Счет в банке-получателе:</label>
        <div class="col-sm-3">
            <?php
            $field = $form['correspondent_bank_account'];
            echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
            ?>
        </div>
        <div class="clearfix visible-sm"></div>
    </div>
</div>

<div class="line">
    <label class="col-sm-3 control-label" for="purpose_payment">Назначение платежа:</label>
    <div class="col-sm-7">
        <?php
        $field = $form['purpose_payment'];
        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
        ?>
    </div>
    <div class="clearfix visible-sm"></div>
</div>

<input type="hidden" name="bank_details_id" value="<?= $form->getObject()->getId() ? $form->getObject()->getId() : -1; ?>"/>

<div class="line">
    <div class="col-sm-9 col-md-offset-3">
        <div class="button_line2">
            <a href="#" class="linkButton" id="bank_block_send_to_manager_link">Заполнить с помощью менеджера</a>
        </div>
        <div class="button_line ">
            <input class="linkButton2" type="submit" value="Сохранить изменения" /><a href="#" class="cancel_edit_bank">Отменить</a>
        </div>
        <div class="small">
            Заявка на изменение будет отправлена вашему менеджеру
        </div>
        <div class="small post_success" style="color: green; display:none"> Заявка успешно отправлена </div>
        <div class="small post_error" style="color: red; display:none"> Не удалось отправить заявку </div>
    </div>
    <div class="clearfix visible-sm"></div>
</div>
