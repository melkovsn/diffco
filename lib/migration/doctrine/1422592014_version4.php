<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version4 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createForeignKey('client', 'client_dm_user_id_dm_user_id', array(
             'name' => 'client_dm_user_id_dm_user_id',
             'local' => 'dm_user_id',
             'foreign' => 'id',
             'foreignTable' => 'dm_user',
             'onUpdate' => '',
             'onDelete' => 'RESTRICT',
             ));
        $this->addIndex('client', 'client_dm_user_id', array(
             'fields' => 
             array(
              0 => 'dm_user_id',
             ),
             ));
    }

    public function down()
    {
        $this->dropForeignKey('client', 'client_dm_user_id_dm_user_id');
        $this->removeIndex('client', 'client_dm_user_id', array(
             'fields' => 
             array(
              0 => 'dm_user_id',
             ),
             ));
    }
}