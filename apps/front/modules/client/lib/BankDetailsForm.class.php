<?php

/**
 * BankDetails form.
 *
 * @package    diffco
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class BankDetailsForm extends BaseBankDetailsForm {

    public function configure() {

        parent::configure();

        $this->disableLocalCSRFProtection();

        $this->setWidgets(array(
            'name_account' => new sfWidgetFormInputText(array(), array('class' => 'form-control required')),
            'recipient' => new sfWidgetFormInputText(array(), array('class' => 'form-control required')),
            'purpose_payment' => new sfWidgetFormTextarea(array(), array('class' => 'form-control required', 'maxlength' => "160")),
            'bank_name' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'bank_address' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'bik' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'bank_kpp' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'bank_inn' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'correspondent_account' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'receiver_settlement_account' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'receiver_inn' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'receiver_kpp' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'beneficiary_bank_name' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'beneficiary_bank_swift_code' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'beneficiary_bank_address' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'beneficiary_bank_account' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'correspondent_bank_name' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'correspondent_bank_swift_code' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'correspondent_bank_address' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
            'correspondent_bank_account' => new sfWidgetFormInputText(array(), array('class' => 'form-control')),
        ));

        $countryChoices = CountryTable::getInstance()->getNamesOfCountries();

        $this->setWidget('beneficiary_bank_country_id', new sfWidgetFormChoice(array('choices' => $countryChoices, 'multiple' => false, 'expanded' => false), array('class' => 'form-control')));
        $this->setValidator('beneficiary_bank_country_id', new sfValidatorChoice(array('choices' => array_keys($countryChoices), 'multiple' => false, 'required' => true)));

        $currencyChoices = CurrencyTable::getInstance()->getNamesOfCurrencies();

        $this->setWidget('currency_id', new sfWidgetFormChoice(array('choices' => $currencyChoices, 'multiple' => false, 'expanded' => false), array('class' => 'form-control')));
        $this->setValidator('currency_id', new sfValidatorChoice(array('choices' => array_keys($currencyChoices), 'multiple' => false, 'required' => true)));
    }

    function bind(array $taintedValues = null, array $taintedFiles = null) {

        $client = DfAuthUser::getInstance()->getAuthUser();

        $taintedValues['client_id'] = $client->getId();

        $this->setValidator('bank_details_id', new sfValidatorString());
        parent::bind($taintedValues, $taintedFiles);
    }

    public function sendMailForManager() {

        $manager = DfAuthUser::getInstance()->getAuthUser()->getManager();

        $aMailData = array(
            'name_account' => (string) $this->getValue('name_account'),
            'recipient' => (string) $this->getValue('recipient'),
            'purpose_payment' => (string) $this->getValue('purpose_payment'),
            'bank_name' => (string) $this->getValue('bank_name'),
            'bank_address' => (string) $this->getValue('bank_address'),
            'bik' => (string) $this->getValue('bik'),
            'bank_kpp' => (string) $this->getValue('bank_kpp'),
            'bank_inn' => (string) $this->getValue('bank_inn'),
            'correspondent_account' => (string) $this->getValue('correspondent_account'),
            'receiver_settlement_account' => (string) $this->getValue('receiver_settlement_account'),
            'receiver_inn' => (string) $this->getValue('receiver_inn'),
            'receiver_kpp' => (string) $this->getValue('receiver_kpp'),
            'beneficiary_bank_name' => (string) $this->getValue('beneficiary_bank_name'),
            'beneficiary_bank_swift_code' => (string) $this->getValue('beneficiary_bank_swift_code'),
            'beneficiary_bank_address' => (string) $this->getValue('beneficiary_bank_address'),
            'beneficiary_bank_account' => (string) $this->getValue('beneficiary_bank_account'),
            'correspondent_bank_name' => (string) $this->getValue('correspondent_bank_name'),
            'correspondent_bank_swift_code' => (string) $this->getValue('correspondent_bank_swift_code'),
            'correspondent_bank_address' => (string) $this->getValue('correspondent_bank_address'),
            'correspondent_bank_account' => (string) $this->getValue('correspondent_bank_account'),
            'client_id' => (string) $this->getValue('client_id'),
            'bank_details_id' => (string) $this->getValue('bank_details_id'),
            'mailto' => (string) $manager->getEmail()
        );

        $mail = $this->getService('mail')
                ->setTemplate('client_bank_details_change_for_manager')
                ->addValues($aMailData)
                ->render();

        $mail->send();
    }

    public function sendRequestMailForManager() {

        /// TODO: логика работы пока непонятна

        $manager = DfAuthUser::getInstance()->getAuthUser()->getManager();

        $aMailData = array(
            'mailto' => (string) $manager->getEmail()
        );

        $mail = $this->getService('mail')
                ->setTemplate('client_bank_details_request_for_manager')
                ->addValues($aMailData)
                ->render();


        $mail->send();
    }

}
