<div class="container index_page">

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h2>Вход</h2>
            <div class="inner-block">
                <div class="row">
                    <div class="main_login" style="display: block;">
                        <form id="login_form" novalidate="novalidate" method="post">
                            <div class="line">
                                <label class="col-sm-3 control-label" for="login">Логин:</label>
                                <div class="col-sm-6">
                                    <input id="login" class="form-control" name="login" type="text" value="">
                                </div>
                                <!--<div class="col-sm-3"><a href="#" class="remember_login">Забыл логин</a></div>-->
                                <div class="clearfix visible-sm"></div>
                            </div>
                            <div class="line">
                                <label class="col-sm-3 control-label" for="password">Пароль:</label>
                                <div class="col-sm-6">
                                    <input id="password" class="form-control" name="password" type="password" value="">                                    
                                </div>
                                <!--<div class="col-sm-3"><a href="#" class="remember_password">Забыл пароль</a></div>-->
                                <div class="clearfix visible-sm"></div>
                            </div>
                            <div class="line">
                                <div class="col-sm-3 col-md-offset-3"><input type="submit" class="linkButton2" value="Войти"></div>

                            </div>
                        </form>
                    </div>

<!--                    <div class="sms_true_block">
                        <form action="#" id="">
                            <div class="line">
                                <label class="control-label col-sm-5" for="kode_true">Код подтверждения:</label>
                                <input id="kode_true" class="form-control col-sm-4" name="kode_true" type="text">
                                <div class="col-sm-2">
                                    <input type="submit" value="ОК" class="linkButton2">
                                </div>
                                <div class="col-sm-2 false_code_text">
                                    Неверный код
                                </div>
                            </div>
                            <div class="line">
                                <label class="control-label  col-sm-5" for="time_kode_true">Срок действия кода:</label>
                                <input id="time_kode_true" class="form-control col-sm-4" type="text" value="2:23">
                                <div class=" col-sm-4"><a href="#" class="more_code">Отправить еще раз</a></div>
                            </div>
                            <div class="code_info">
                                <p>На мобильный телефон, привязанный к вашему аккаунту,<br>должно прийти SMS оповещение с кодом подтверждения</p>
                            </div>
                        </form>
                    </div>

                    <div class="block_remember_password" style="display: none;">
                        <form action="#" id="block_remember_password" novalidate="novalidate">
                            <button type="button" class="close_icon back_to_login"></button>
                            <h3>Восстановление пароля</h3>
                            <div class="line">
                                <label class="col-sm-3 control-label" for="login">Логин:</label>
                                <div class="col-sm-6">
                                    <input id="login" class="form-control" name="login" type="text" value="">
                                </div>
                                <div class="col-sm-3"><a href="#" class="remember_login">Забыл логин</a></div>
                                <div class="clearfix visible-sm"></div>
                            </div>
                            <div class="line">
                                <div class="col-sm-3 col-md-offset-3"><input type="submit" class="linkButton2" value="Восстановить"></div>

                            </div>
                        </form>
                    </div>

                    <div class="sms_remember_password">
                        <form action="#" id="sms_remember_password" novalidate="novalidate">
                            <h3>Восстановление пароля</h3>
                            <div class="line">
                                <label class="control-label col-sm-4" for="kode_true">Код подтверждения:</label>
                                <input id="kode_true" class="form-control col-sm-4" name="kode_true" type="text">
                                <div class="col-sm-4">
                                    <input type="submit" value="ОК" class="linkButton2">
                                </div>
                            </div>
                            <div class="line">
                                <label class="control-label  col-sm-4" for="time_kode_true">Срок действия кода:</label>
                                <input id="time_kode_true" class="form-control col-sm-4" type="text" value="2:23">
                                <div class=" col-sm-4"><a href="#" class="more_code">Отправить еще раз</a></div>
                            </div>
                            <div class="code_info">
                                <p>На мобильный телефон, привязанный к вашему аккаунту,<br>должно прийти SMS оповещение с кодом подтверждения</p>
                            </div>
                        </form>
                    </div>

                    <div class="find_login" style="display: none;">
                        <button type="button" class="close_icon back_to_login"></button>
                        <h3>Поиск логина</h3>
                        <p>Имя вашего логина вы можете найти в своем клиентском договоре в разделе 4.1. Обычно это ваша фамилия и инициалы набранные латиницей. Например, SPIVANOV.</p>
                        <p>Если вам не удалось найти логин в договоре, пожалуйста, свяжитесь с нашим офисом по телефону +7 (495) 111-11-11</p>
                        <a href="#" class="back_to_login">Вернуться назад</a>
                    </div>-->

                </div>
            </div>
        </div>
    </div>
</div>