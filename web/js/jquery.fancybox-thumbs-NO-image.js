 /*!
 * Thumbnail helper for fancyBox
 * version: 1.0.2
 * @requires fancyBox v2.0 or later
 *
 * Usage: 
 *     $(".fancybox").fancybox({
 *         thumbs: {
 *             width	: 50,
 *             height	: 50
 *         }
 *     });
 * 
 * Options:
 *     width - thumbnail width
 *     height - thumbnail height
 *     source - function to obtain the URL of the thumbnail image
 *     position - 'top' or 'bottom'
 * 
 */
(function ($) {
	//Shortcut for fancyBox object
	var F = $.fancybox;

	//Add helper object
	F.helpers.thumbs = {
		wrap: null,
		list: null,
		width: 0,
		
		//Default function to obtain the URL of the thumbnail image
		source: function (el) {
			var img = $(el).find('img');

			return img.length ? img.attr('src') : el.href;
		},

		init: function (opts) {
		},

		//Center list
		update: function (opts) {
			if (this.list) {
			}
		},

		beforeLoad: function (opts) {
			//Remove self if gallery do not have at least two items 
			if (this.list) {
				console.log($('.fancybox-skin').width());
				this.list.stop(true).animate({
					'left': Math.floor($('.fancybox-skin').width() * 0.5 - (this.width * (F.group.length) * 0.5))
				}, 150);
			}
			if (F.group.length < 2) {
				F.coming.helpers.thumbs = false;

				return;
			}

			//Increase bottom margin to give space for thumbs
			F.coming.margin[ opts.position === 'top' ? 0 : 2 ] = opts.height + 30;
		},

		afterShow: function (opts) {
			//Check if exists and create or update list
			if (this.list) {
				this.update(opts);

			} else {
				this.init(opts);
			}

			//Set active element
		},

		onUpdate: function () {
			this.update();
		},

		beforeClose: function () {
			if (this.wrap) {
				this.wrap.remove();
			}

			this.wrap = null;
			this.list = null;
			this.width = 0;
		},
		beforeShow: function (opts) {
			var that = this,
				list,
				active,
				type = opts.type || 'inside',
				padding = opts.padding || 5,
				thumbWidth = opts.width || 50,
				thumbHeight = opts.height || 50,
				thumbSource = opts.source || this.source;

			//Build list structure
			list = '';
			thumbs = '';

			for (var n = 0; n < F.group.length; n++) {
//				list += '<li><a style="width:' + thumbWidth + 'px;height:' + thumbHeight + 'px;" href="javascript:jQuery.fancybox.jumpto(' + n + ');"></a></li>';
				active =  (n == F.current.index) ? ' class="active"' : '';
				list += '<li' + active + '><a href="javascript:jQuery.fancybox.jumpto(' + n + ');">'+(n+1)+'</a></li>';
			}
			
			var left = (F.coming.width * 0.5) - ((thumbWidth + padding*2 + 2) * F.group.length * 0.5);
			
			thumbs = $('<div class="pagination"><ul>' + list + '</ul></div>');

			/*this.width = this.list.children().eq(0).outerWidth();
			this.list.width(this.width * (F.group.length)).css('left', Math.floor($('.fancybox-skin').width() * 0.5 - (this.width * (F.group.length) * 0.5)));*/
			
			switch (type) {
				case 'inside':
					target = F.skin;
				break;

				case 'outside':
					target = F.wrap;
				break;

				case 'over':
					target = F.inner;
				break;

				default: // 'float'
				break;
			}

			thumbs['appendTo'](target);
			
		}
	}

}(jQuery));