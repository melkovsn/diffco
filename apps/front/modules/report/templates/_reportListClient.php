<?php use_javascript('/bootstrap/js/jasny-bootstrap.min.js'); ?>
<?php use_javascript('/bootstrap/js/moment.js'); ?>
<?php use_javascript('/bootstrap/js/bootstrap-datetimepicker.js'); ?>
<?php use_javascript('/bootstrap/js/bootstrap-datetimepicker.ru.js'); ?>
<?php use_javascript('/js/client/reports.js'); ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Отчеты</h1>
        </div>
    </div>
</div>

<div class="container">
    <div class="inner-block">
        <div class="row">
            <form action="#" id="reports_search" >
                <div class="col-md-12">
                    <div class="line">
                        <div class="col-sm-8">
                            <label class="control-label period">Период </label>
                            <input id="start_date" class="date form-control" name="start_date" type="text" value="<?= $startDate ?>" data-date-format="DD.MM.YYYY"/>
                            По 
                            <input id="end_date" class="date form-control" name="end_date" type="text" value="<?= $endDate ?>" data-date-format="DD.MM.YYYY"/> 
                            <input type="submit" value="Показать" class="linkButton2" />								
                        </div>
                        <div class="col-sm-4 reports_search_links">
                            <?php
                                $monthStartDate = date("d.m.Y", strtotime("-1 month"));
                                $quartalStartDate = date("d.m.Y", strtotime("-3 month"));
                                $yearStartDate = date("d.m.Y", strtotime("-1 year"));
                                $endDate = date("d.m.Y");
                            ?>
                            <a href="#" class="period_control" data-start_date="<?= $monthStartDate ?>" data-end_date="<?= $endDate; ?>">За месяц</a>
                            <a href="#" class="period_control" data-start_date="<?= $quartalStartDate ?>" data-end_date="<?= $endDate; ?>">За квартал</a>
                            <a href="#" class="period_control" data-start_date="<?= $yearStartDate ?>" data-end_date="<?= $endDate; ?>">За год</a>
                        </div>
                        <div class="clearfix visible-sm"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container" id="reports_search_result">
</div>

<?php slot('popup') ?>

<div class="modal fade" id="code_true_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data_id="">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <button type="button" class="close_icon" data-dismiss="modal" aria-hidden="true"></button>
            <form action="#" id="confirm_code_form">
                <div class="line">
                    <label class="control-label" for="confirm_code">Код подтверждения:</label>
                    <input id="confirm_code" class="form-control" name="confirm_code" type="text" autocomplete="off"/>
                    <input type="submit" value="ОК" class="linkButton2"/>
                </div>
                <div class="line">
                    <label class="control-label" for="confirm_code_time">Срок действия кода:</label>
                    <input id="confirm_code_time" class="form-control" type="text" name="confirm_code_time" disabled="disabled" value=""/>
                    <a href="#" class="more_code">Отправить еще раз</a>
                </div>
                <div class="code_info">
                    <p>На мобильный телефон, привязанный к вашему аккаунту,<br/>должно прийти SMS оповещение с кодом подтверждения</p>
                </div>
            </form>

        </div>
    </div>
</div>

<?php end_slot(); ?>