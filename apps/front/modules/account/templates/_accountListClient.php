<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h1>Счета и портфели</h1>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="inner-block">
                <div class="table_block with_border portfolios_table">
                    <table>
                        <thead>
                            <tr>
                                <td>Номер счета</td>
                                <td>Тип счета</td>
                                <td>Тарифный план</td>
                                <td>Текущая стоимость</td>
                                <td>Дата обновления</td>
                                <td class="with_button">Просмотр</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($currencyAccounts as $account) : ?>
                                <tr>
                                    <td><?= $account->getAccountNumber(); ?></td>
                                    <td class="name_item"><?= $account->getAccountType(); ?> (<?= $account->getCurrency()->getName(); ?>)</td>
                                    <td><?= $account->getTariffPlan()->getName(); ?></td>
                                    <td><?= $account->getAmount(); ?> <?= $account->getCurrency()->getSign(); ?></td>
                                    <td><?= $account->getUpdateDateFormatted(); ?></td>
                                    <td class="with_button"><a href="report_item.html" class="linkButton">Текущий</a></td>
                                </tr>
                            <?php endforeach; ?>
                            <?php foreach ($depoAccounts as $account) : ?>
                                <tr>
                                    <td><?= $account->getAccountNumber(); ?></td>
                                    <td class="name_item">Депозитарный<div id="item_<?= $account->getId(); ?>" class="show_more"><a href="#">Развернуть</a></div><div class="hide_more"><a href="#">Свернуть</a></div></td>
                                    <td><?= $account->getTariffPlan()->getName(); ?></td>
                                    <td><?= $account->getAmount(); ?> <?= $account->getCurrency()->getSign(); ?></td>
                                    <td><?= $account->getUpdateDateFormatted(); ?></td>
                                    <td class="with_button"><a href="report_item.html" class="linkButton">Текущий</a></td>
                                </tr>
                                <tr class="hidden_table" rel="item_<?= $account->getId(); ?>">
                                    <td></td>
                                    <th>Название акции</th>
                                    <th>Количество</th>
                                    <th>Текущая стоимость</th>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php foreach ($account->getStocks() as $stock) : ?>
                                    <tr class="hidden_table" rel="item_<?= $account->getId(); ?>">
                                        <td></td>
                                        <th><?= $stock->getStockName(); ?></th>
                                        <th><?= $stock->getStockNumber(); ?></th>
                                        <th><?= $stock->getAmount(); ?> <?= $stock->getCurrency()->getSign(); ?></th>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endforeach; ?>    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>