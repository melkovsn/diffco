<div class="row">
    <div class="col-md-12">
        <div class="inner-block">
            <div class="table_block">
                <table>
                    <thead>
                        <tr>
                            <td>Счёт или портфель</td>
                            <td>Период</td>
                            <td>Дата получения</td>
                            <td class="with_button">Подпись</td>
                            <td class="with_button">Просмотр</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($reports as $report) : ?>
                            <tr>
                                <td><?= $report['Account']['account_number'] ?></td>
                                <td><?= date_format(date_create($report['start_date']), 'd.m.Y') ?> - <?= date_format(date_create($report['end_date']), 'd.m.Y') ?></td>
                                <td><?= date_format(date_create($report['report_date']), 'd.m.Y') ?></td>

                                <?php if ($report['is_signed']) : ?>
                                    <td class="with_button">Подписан</td>
                                <?php else : ?>
                                    <td class="with_button"><a href="#" class="linkButton2 subscribe" data_id="<?= $report['id'] ?>">Подписать</a></td>
                                <?php endif; ?>

                                <td class="with_button"><a href="<?= url_for('@reportDetail?id=' . $report['id']); ?>" class="linkButton">Смотреть</a></td>
                            </tr>
                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>