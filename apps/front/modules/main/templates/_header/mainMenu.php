<?php
$currentSlug = sfContext::getInstance()->getPage()->getSlug();
$menuItems = sfConfig::get('app_main_menu');
$menuItems = $menuItems[$type];

$currentMainIndex = 0;
?>

<div class="row top_menu">
    <div class="col-sm-8">
        <ul class="nav navbar-nav">
            <?php foreach ($menuItems as $key => $item) : ?>
                <?php
                $isActive = false;
                $isClickable = true;

                if ($currentSlug == $item['link']) {
                    $isActive = true;
                    $isClickable = false;
                    $currentMainIndex = $key;
                } elseif (isset($item['subitems'])) {

                    foreach ($item['subitems'] as $subItem) {
                        if ($currentSlug == $subItem['link']) {
                            $isActive = true;
                            $currentMainIndex = $key;
                        }
                    }
                } elseif (strpos($currentSlug, $item['link']) === 0) {
                    $isActive = true;
                }
                ?>
                <li <?php print $isActive ? ' class="active"' : '' ?>>
                    <?php if ($isClickable) : ?>
                        <a href="/<?= $item['link'] ?>"><?= $item['name'] ?></a>
                    <?php else : ?>
                        <a><?= $item['name'] ?></a>	
                    <?php endif; ?>
                </li>	

            <?php endforeach; ?>
        </ul>
    </div>
    <div class="col-sm-4 go_to_site">
        <a target="_blank" href="#">Перейти на сайт компании</a>
    </div>
</div>

<?php if (isset($menuItems[$currentMainIndex]) && isset($menuItems[$currentMainIndex]['subitems'])) : ?>
    <div class="row middle_menu">
        <div class="col-sm-12">
            <ul class="nav navbar-nav">
                <?php foreach ($menuItems[$currentMainIndex]['subitems'] as $subitem) : ?>

                    <?php if ($currentSlug == $subitem['link']) : ?>
                        <li class="active">
                            <a><?= $subitem['name'] ?></a>
                        </li>	
                    <?php else : ?>
                        <li>
                            <a href="/<?= $subitem['link'] ?>"><?= $subitem['name'] ?></a>
                        </li>	
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
<?php endif; ?>