<?php include_component('user', 'checkAuth', array('type'=>'administrator'));?>

<div class="page-wrapper <?php $isEditMode && print ' edit' ?>" id="dm_page">
    <div class="main_container dm_layout">

        <div class="navbar-wrapper">

            <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="navbar-header">
                    <div class="container">
                        <div class="row header_line">
                            <?php include_partial('main/header/logo'); ?>							
                            <?php include_component('main','administratorInfo'); ?>						
                        </div>
                        <div class="row">
                            <div class="col-sm-12"><div class="red_line"></div></div>
                        </div>
                    </div>		
                </div>
                <div class="navbar-collapse collapse">
                    <div class="container">
                        <?php include_partial('main/header/mainMenu', array('type' => 'administrator')); ?>
                    </div>
                </div>
            </div>

        </div>
        
        <?php echo $helper->renderArea('page.content') ?>
        
        <div class="clear-footer"></div>
    </div>
</div>

<div id="footer">

    <!-- FOOTER -->
    <?php include_partial('main/footer'); ?>	
</div>
