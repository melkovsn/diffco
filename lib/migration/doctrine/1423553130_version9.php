<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version9 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->changeColumn('agreement', 'client_id', 'integer', '8', array(
             'notnull' => '',
             ));
        $this->createTable('manager', array(
             'id' => 
             array(
              'type' => 'integer',
              'length' => '8',
              'autoincrement' => '1',
              'primary' => '1',
             ),
             'name' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '128',
             ),
             'surname' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '128',
             ),
             'patronymic' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '128',
             ),
             'email' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'unique' => '1',
              'length' => '128',
             ),
             'dm_user_id' => 
             array(
              'type' => 'integer',
              'notnull' => '1',
              'length' => '8',
             ),
             'login' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '128',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             'collate' => 'utf8_unicode_ci',
             'charset' => 'utf8',
             ));
        $this->addColumn('agreement', 'manager_id', 'integer', '8', array(
             'notnull' => '',
             ));
    }

    public function down()
    {
        $this->dropTable('manager');
        $this->removeColumn('agreement', 'manager_id');
    }
}