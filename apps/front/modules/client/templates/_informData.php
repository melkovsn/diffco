<div class="inner-block">
    <div class="row">
        <form action="#" id="inform_data_form">
            <div class="col-md-12 fields">
                <div class="line">
                    <label class="col-sm-3 control-label big_text" for="phone_sms_info">Телефон для SMS информирования<br><span class="small">(Для пароля не используется)</span></label>
                    <div class="col-sm-3">
                        <input id="phone_sms_info" class="form-control" data-mask="+9 999 999-99-99" name="phone_sms_info" type="text" value="<?= $client->getPhoneSmsInfo(); ?>" default-value="value="<?= $client->getPhoneSmsInfo(); ?>" disabled="disabled">
                    </div>
                    <div class="col-sm-6 contact_controls">
                        <a href="#" class="edit_phone edit_field">Изменить</a>
                        <a href="#" class="linkButton2 save_contact" data_field="phone_sms_info">Сохранить</a>
                        <a href="#" class="linkButton cancel_contact">Отменить</a>
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
                <div class="line">
                    <label class="col-sm-3 control-label" for="email">E-mail</label>
                    <div class="col-sm-3">
                        <input id="email" class="form-control" name="email" type="text" value="<?= $client->getEmail(); ?>" default-value="<?= $client->getEmail(); ?>" disabled="disabled">
                    </div>
                    <div class="col-sm-6 contact_controls">
                        <a href="#" class="edit_email edit_field">Изменить</a>
                        <a href="#" class="linkButton2 save_contact" data_field="email">Сохранить</a>
                        <a href="#" class="linkButton cancel_contact">Отменить</a>
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
            </div>
        </form>
    </div>
</div>