<?php

class DfAuthUser {

    protected static $instance;
    protected static $authUser;
    protected static $sessionId = 'USERID';

    public static function getInstance() {
        if (self::$instance === NULL) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private static function getSessionId() {

        return self::$sessionId;
    }

    public function getUserId() {
        if ($this->isAuth()) {
            return (int) sfContext::getInstance()->getUser()->getAttribute($this->getSessionId());
        }
        return 0;
    }

    public function login($sLogin, $sPassword, $bRememberMe = false) {

        $userId = $this->findUser($sLogin, $sPassword);

        if ($userId) {
            sfContext::getInstance()->getUser()->setAttribute($this->getSessionId(), $userId);
        }

        return $userId;
    }

    public function logout() {

        sfContext::getInstance()->getUser()->getAttributeHolder()->remove($this->getSessionId());
        self::$authUser = NULL;
    }

    public function findUser($login = "", $password = "") {

        if ($login) {
            $client = ClientTable::getInstance()->findOneByLogin($login);

            if ($client) {
                $user = $client->getDmUser();

                if ($user->checkPassword($password)) {
                    return $user->getId();
                }
            }
        }
        return 0;
    }

    public function getAuthUser() {
        
        if (!$this->getUserId()){
            self::$authUser = NULL;
        }
        
        if (self::$authUser === NULL) {
        
            $authUser = ClientTable::getInstance()->findOneByDmUserId($this->getUserId());

            if (!$authUser) {

                ///TODO проверка других типов
            }
            self::$authUser = $authUser;
        }
        
        return self::$authUser;
    }

    public function isAuth() {

        $bAuth = false;

        if (sfContext::hasInstance()) {
            $sSessionId = sfContext::getInstance()->getUser()->getAttribute($this->getSessionId());
            if ($sSessionId) {
                $oUser = $this->getUserById($sSessionId);
                if ($oUser && $oUser->getIsActive()) {
                    $bAuth = true;
                }
            }
        }

        return $bAuth;
    }

    public function getUserById($id) {
        $user = DmUserTable::getInstance()->findOneById($id);

        return $user;
    }

    public function getCurrentUser() {

        $id = $this->getUserId();
        $user = $this->getUserById($id);

        return $user;
    }

}

?>
