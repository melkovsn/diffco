<?php

/**
 * Пользователи actions
 */
class userActions extends myFrontModuleActions {

    public function executeLogout() {

        DfAuthUser::getInstance()->logout();
        $this->redirect('/login');
    }
    
    public function executeSendConfirmCode(dmWebRequest $request) {

        $status = 'error';
        
        if ($request->isMethod('POST')) {

            $client = DfAuthUser::getInstance()->getAuthUser();
            
            /// TODO: разобраться, на какой номер нужно в действительности отсылать смс
            $phone = $client->getPhoneSMSInfo();
            
            if (ConfirmCodeService::getInstance()->sendConfirmCode($phone)){                
                $status = 'success';
            }
        }
        
        return $this->renderText(json_encode(array('status' => $status, 'time_alive'=>300)));
    }
    
}
