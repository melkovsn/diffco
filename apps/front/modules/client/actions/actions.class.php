<?php

/**
 * Клиент actions
 */
class clientActions extends myFrontModuleActions {

    public function executeSendPersonalData(dmWebRequest $request) {

        $status = 'error';

        if ($request->isMethod('POST')) {

            $client = DfAuthUser::getInstance()->getAuthUser();

            if (!$client || !($client->getType() == 'client')) {
                return $this->renderText('error');
            }

            $this->form = new ClientPassportDataFeedbackForm();

            $this->form->bind($request->getPostParameters(), $request->getFiles());

            if ($this->form->isValid()) {
                $this->form->sendMailForManager();
                $status = 'success';
            }
        }

        return $this->renderText($status);
    }

    public function executeSaveField(dmWebRequest $request) {

        $status = 'error';

        if ($request->isMethod('POST')) {

            $client = DfAuthUser::getInstance()->getAuthUser();

            if ($client && ($client->getType() == 'client')) {

                $code = $request->getPostParameter('confirm_code', 0);
                $field = $request->getPostParameter('field');
                $value = $request->getPostParameter('value');

                if (ConfirmCodeService::getInstance()->checkConfirmCode($code, 300)) {

                    if ($field == 'phone_sms_info') {
                        if (filter_var($value, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^\+\d\s\d{3}\s\d{3}-\d{2}-\d{2}/")))) {

                            $client->setPhoneSmsInfo($value);
                            $client->save();
                            $status = "success";
                        }
                    } elseif ($field == 'email') {

                        if (filter_var($value, FILTER_VALIDATE_EMAIL)) {

                            $client->setEmail($value);
                            $client->save();
                            $status = "success";
                        }
                    }
                } else {
                    $status = 'code_error';
                }
            }
        }

        return $this->renderText(json_encode(array('status' => $status)));
    }

    public function executeRenderBankDetailsForm(dmWebRequest $request) {

        if (!$request->isMethod('POST')) {
            return $this->renderText('');
        }

        $client = DfAuthUser::getInstance()->getAuthUser();

        if (!$client || !($client->getType() == 'client')) {
            return $this->renderText('');
        }

        $bankDetailsId = $request->getPostParameter('bankDetailsId', -1);

        if ($bankDetailsId == -1) {
            return $this->renderText('');
        }

        $bankDetails = BankDetailsTable::getInstance()->getBankDetailsByIdAndClientId($bankDetailsId, $client->getId());

        $this->form = new BankDetailsForm($bankDetails);

        sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');

        return $this->renderText(get_partial('renderBankDetailsForm', array('form' => $this->form)));
    }

    public function executeSendBankDetails(dmWebRequest $request) {

        $status = 'error';

        if ($request->isMethod('POST')) {

            $client = DfAuthUser::getInstance()->getAuthUser();

            if (!$client || !($client->getType() == 'client')) {
                return $this->renderText('error');
            }

            $this->form = new BankDetailsForm();

            $this->form->bind($request->getPostParameters());

            if ($this->form->isValid()) {
                $this->form->sendMailForManager();
                $status = 'success';
            }
        }

        return $this->renderText($status);
    }

    public function executeSendBankDetailsRequestToManager(dmWebRequest $request) {

        $status = 'error';

        if ($request->isMethod('POST')) {

            $client = DfAuthUser::getInstance()->getAuthUser();

            if (!$client || !($client->getType() == 'client')) {
                return $this->renderText('error');
            }

            $this->form = new BankDetailsForm();
            $this->form->sendRequestMailForManager($request->getPostParameters());

            $status = 'success';
        }

        return $this->renderText($status);
    }

    public function executeSearchReports(dmWebRequest $request) {

        if (!$request->isMethod('POST')) {
            return $this->renderText('');
        }

        $client = DfAuthUser::getInstance()->getAuthUser();

        if (!$client || !($client->getType() == 'client')) {
            return $this->renderText('');
        }

        $startDate = $request->getPostParameter('start_date');
        $endDate = $request->getPostParameter('end_date');

        $reports = $client->getReportsByPeriod($startDate, $endDate, true);

        sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');

        return $this->renderText(get_partial('renderReportsList', array('reports' => $reports)));
    }

    public function executeSignReport(dmWebRequest $request) {

        $status = 'error';

        if ($request->isMethod('POST')) {

            $client = DfAuthUser::getInstance()->getAuthUser();

            if ($client && ($client->getType() == 'client')) {

                $reportId = $request->getPostParameter('reportId');
                $code = $request->getPostParameter('confirm_code', 0);

                if (ConfirmCodeService::getInstance()->checkConfirmCode($code, 300)) {
                    $report = ReportTable::getInstance()->findOneById($reportId);
                    $report->setIsSigned(true);
                    $report->save();

                    $status = 'success';
                } else {
                    $status = 'code_error';
                }
            }
        }

        return $this->renderText(json_encode(array('status' => $status)));
    }

}
