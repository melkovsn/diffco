<?php

/**
 * Пользователи components
 * 
 * No redirection nor database manipulation ( insert, update, delete ) here
 */
class userComponents extends myFrontModuleComponents {

    public function executeAuth(dmWebRequest $request) {

        if (DfAuthUser::getInstance()->isAuth()) {

            $user = DfAuthUser::getInstance()->getAuthUser();

            $sectionUrl = $user->getHomeUrl();

            $this->getController()->redirect($sectionUrl, 0, 302);
        }

        $this->form = new DfLoginForm($request->getPostParameters());

        if (($request->getMethod() == 'POST')) {
            
            $this->form->bind($request->getPostParameters());
            
            if ($this->form->isValid()) {
 
                DfAuthUser::getInstance()->login($request->getPostParameter('login'), $request->getPostParameter('password'));
                
                $this->getController()->redirect('/login', 0, 302);
            }
        }
    }

    public function executeCheckAuth() {

        if (DfAuthUser::getInstance()->isAuth()) {
            $user = DfAuthUser::getInstance()->getAuthUser();

            if ($user->getType() != $this->type) {

                $sectionUrl = $user->getHomeUrl();
                $this->getController()->redirect($sectionUrl, 0, 302);
            }
        } else {
            $this->getController()->redirect('/login', 0, 302);
        }
    }

}
