<?php use_javascript('/bootstrap/js/jasny-bootstrap.min.js'); ?>
<?php use_javascript('/bootstrap/js/moment.js'); ?>
<?php use_javascript('/bootstrap/js/bootstrap-datetimepicker.js'); ?>
<?php use_javascript('/bootstrap/js/bootstrap-datetimepicker.ru.js'); ?>
<?php use_javascript('/js/upload_file.js'); ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Ваши данные</h1>
        </div>
    </div>
</div>

<div class="container">    
    <?php include_component('client', 'passportData'); ?>
    <?php include_component('client', 'informData'); ?>
    <?php include_component('client', 'bankDetails'); ?>
</div>

<?php slot('popup') ?>

<div class="modal fade" id="code_true_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-field="">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <button type="button" class="close_icon" data-dismiss="modal" aria-hidden="true"></button>
            <form action="#" id="confirm_code_form">
                <div class="line">
                    <label class="control-label" for="confirm_code">Код подтверждения:</label>
                    <input id="confirm_code" class="form-control" name="confirm_code" type="text" autocomplete="off"/>
                    <input type="submit" value="ОК" class="linkButton2"/>
                </div>
                <div class="line">
                    <label class="control-label" for="confirm_code_time">Срок действия кода:</label>
                    <input id="confirm_code_time" class="form-control" type="text" name="confirm_code_time" disabled="disabled" value=""/>
                    <a href="#" class="more_code">Отправить еще раз</a>
                </div>
                <div class="code_info">
                    <p>На мобильный телефон, привязанный к вашему аккаунту,<br/>должно прийти SMS оповещение с кодом подтверждения</p>
                </div>
            </form>

        </div>
    </div>
</div>

<?php end_slot(); ?>