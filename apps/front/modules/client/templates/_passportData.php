<?php use_javascript('/js/client/personal.js'); ?>

<div class="inner-block">
    <div class="row">
        <form action="#" id="personal_data" method="POST" enctype="multipart/form-data">
            <div class="col-md-12">
                <div class="inner-top">
                    <h3>Паспортные данные</h3>
                    <a href="#" class="show_edit">Изменить</a>
                    <a class="close_data_edit close_icon" href="#"></a>
                </div>

            </div>
            <div class="clearfix visible-md"></div>
            <div class="col-md-6 fields">
                <div class="line">
                    <label class="col-sm-3 control-label" for="surname">Фамилия</label>
                    <div class="col-sm-9">
                        <?php
                        $field = $form['surname'];
                        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
                        ?>
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
                <div class="line">
                    <label class="col-sm-3 control-label" for="name">Имя</label>
                    <div class="col-sm-9">
                        <?php
                        $field = $form['name'];
                        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
                        ?>
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
                <div class="line">
                    <label class="col-sm-3 control-label" for="patronymic">Отчество</label>
                    <div class="col-sm-9">
                        <?php
                        $field = $form['patronymic'];
                        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
                        ?>  
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
                <div class="line">
                    <label class="col-sm-3 control-label short_height" for="birth_date">Дата рождения</label>
                    <div class="col-sm-4">
                        <?php
                        $field = $form['birth_date'];
                        echo $field->getWidget()->render($field->getName(), date_format(date_create($field->getValue()), 'd.m.Y'), $field->getWidget()->getAttributes());
                        ?>  
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
                <div class="line">
                    <label class="col-sm-3 control-label" for="passport_scan">Скан паспорта</label>
                    <div class="col-sm-9">
                        <div class="file_image">
                            <img src="<?= $form['passport_scan']->getValue(); ?>" width="200" alt="" />
                        </div>								
                        <div class="files">
                            <label>Приложить файл:</label>
                            <div id="file">
                                <?php
                                $field = $form['passport_scan'];
                                echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
                                ?>  
                            </div>

                            <ul id="file_list"></ul>
                        </div>
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
                <div class="line show_edit_block">
                    <div class="col-sm-9 col-md-offset-3">
                        <div class="button_line ">
                            <input class="linkButton2" type="submit" value="Сохранить изменения" /><a href="#" class="cancel">Отменить</a>
                        </div>
                        <div class="small">
                            Заявка на изменение будет отправлена вашему менеджеру
                        </div>
                        <div class="small post_success" style="color: green; display:none">
                            Заявка успешно отправлена
                        </div>
                        <div class="small post_error" style="color: red; display:none">
                            Не удалось отправить заявку
                        </div>
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
                
            </div>
            <div class="col-md-6 fields">
                <div class="line">
                    <label class="col-sm-3 control-label" for="country_id">Гражданство</label>
                    <div class="col-sm-5">
                        <?php
                        $field = $form['country_id'];
                        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
                        ?> 
                    </div>
                    <div class="col-sm-4">
                        <?php
                        $field = $form['is_resident'];
                        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
                        ?> 
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
                <div class="line">
                    <label class="col-sm-3 control-label" for="passport_series">Серия</label>
                    <div class="col-sm-2">
                        <?php
                        $field = $form['passport_series'];
                        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
                        ?>  
                    </div>
                    <label class="col-sm-2 control-label" for="passport_number">Номер</label>
                    <div class="col-sm-5">
                        <?php
                        $field = $form['passport_number'];
                        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
                        ?>  
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
                <div class="line">
                    <label class="col-sm-3 control-label" for="passport_date">Выдан</label>
                    <div class="col-sm-4">
                        <?php
                        $field = $form['passport_date'];
                        echo $field->getWidget()->render($field->getName(), date_format(date_create($field->getValue()), 'd.m.Y'), $field->getWidget()->getAttributes());
                        ?>  </div>
                    <div class="col-sm-5">
                        <?php
                        $field = $form['place_of_issue'];
                        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
                        ?>  
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
                <div class="line">
                    <label class="col-sm-3 control-label" for="registration">Прописан/а</label>
                    <div class="col-sm-9">
                        <?php
                        $field = $form['registration'];
                        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
                        ?>  
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
                <div class="line">
                    <label class="col-sm-3 control-label short_height" for="post_address">Почтовый адрес</label>
                    <div class="col-sm-9">
                        <?php
                        $field = $form['post_address'];
                        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
                        ?>  </div>
                    <div class="clearfix visible-sm"></div>
                </div>
                <div class="line">
                    <label class="col-sm-3 control-label short_height" for="birth_place">Место рождения</label>
                    <div class="col-sm-9">
                        <?php
                        $field = $form['birth_place'];
                        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
                        ?>  
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
                <div class="line">
                    <label class="col-sm-3 control-label" for="inn">ИНН</label>
                    <div class="col-sm-9">
                        <?php
                        $field = $form['inn'];
                        echo $field->getWidget()->render($field->getName(), $field->getValue(), $field->getWidget()->getAttributes());
                        ?>  
                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>
                <div class="line">
                    <div class="col-sm-9 col-sm-offset-3 errors">

                    </div>
                    <div class="clearfix visible-sm"></div>
                </div>

            </div>
        </form>
    </div>
</div>
