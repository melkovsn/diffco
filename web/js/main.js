$(document).ready(function() {
	/*var retina_size;
	window.devicePixelRatio === undefined ? 1 : window.devicePixelRatio;
	if (window.devicePixelRatio != 1){
		retina_size = 1;
	}else{
		retina_size = 2;
	}
	
	//1 - экран ретинистый, 2 - экран обычный
    jQuery.cookie('retina_size', retina_size);
	*/
	
	$('a.print').click(function(){
		window.print();
		return false;
	});
	
//	if($('input[name = type_operation]:checked').length > 0){
//		var t = $('input[name = type_operation]:checked');
//		if ($(t).attr('id') == 'type_operation_1'){
//			$('.bank_details').slideDown();
//		}else{
//			$('.bank_details').slideUp();
//		}
//	}
//	
//	$('input[name = type_operation]').change(function(){
//		if ($(this).attr('id') == 'type_operation_1'){
//			$('.bank_details').slideDown();
//		}else{
//			$('.bank_details').slideUp();
//		}
//	});
	
//	$('#like_fact').change(function(){
//		if ($(this).is(':checked')){
//			$('#post_address').attr('disabled','disabled').val($('#fact_address').val());
//		}else{
//			$('#post_address').removeAttr('disabled');
//		}
//	});
//
//	$('input[name = like_fact]').change(function(){
//		if ($(this).attr('id') == 'type_operation_1'){
//			$('.bank_details').slideDown();
//		}else{
//			$('.bank_details').slideUp();
//		}
//	});

	if($('input[name = trade_sector_from]:checked').length > 0){
		var t = $('input[name = trade_sector_from]:checked');
		if ($(t).attr('id') == 'trade_sector_from_1'){
			$('#trade_sector_to_2').prop('checked', true);
		}else{
			$('#trade_sector_to_1').prop('checked', true);
		}
	}
	
	$('input[name = trade_sector_from]').change(function(){
		if ($(this).attr('id') == 'trade_sector_from_1'){
			$('#trade_sector_to_2').prop('checked', true);
		}else{
			$('#trade_sector_to_1').prop('checked', true);
		}
	});
	
	$('input[name = trade_sector_to]').change(function(){
		if ($(this).attr('id') == 'trade_sector_to_1'){
			$('#trade_sector_from_2').prop('checked', true);
		}else{
			$('#trade_sector_from_1').prop('checked', true);
		}
	});

	$('.remember_login').on('click',function(){
		$('.main_login').slideUp();
		$('.block_remember_password').slideUp();
		$('.find_login').slideDown();
		return false;
	});
	
	$('.remember_password').on('click',function(){
		$('.main_login').slideUp();
		$('.block_remember_password').slideDown();
		return false;
	});
	
	$('.back_to_login').on('click',function(){
		$('.main_login').slideDown();
		$('.block_remember_password').slideUp();
		$('.find_login').slideUp();
		return false;
	});
	
	
	$('select').selectpicker();
	
	if($('.fileinput').length > 0){
		$('.fileinput').fileinput()
	}
	
	$('.save_client').on('click',function(){
		$('#personal_data').submit();
		return false;
	});
	
	$('.date_sort, .company_sort, .fio_sort, .manager_sort, .login_sort').on('click',function(){
		$(this).toggleClass('glyphicon-chevron-up');
		$(this).toggleClass('glyphicon-chevron-down');
		return false;
	});
	
	$('#all_clients').change(function(){
		if ($(this).is(':checked')){
			$('.client_checkbox').prop('checked', true);
		}else{
			$('.client_checkbox').prop('checked', false);
		}
	});
	
	$('.client_checkbox').change(function(){
		if (!$(this).is(':checked')){
			$('#all_clients').prop('checked', false);
		}
	});
	
	
	
	if($('#birth_date').length > 0){
		$('#birth_date').datetimepicker({
			language: 'ru',
			pickTime: false
		});
	}
    
    if($('#passport_date').length > 0){
		$('#passport_date').datetimepicker({
			language: 'ru',
			pickTime: false
		});
	}
	
	if($('#date_give').length > 0){
		$('#date_give').datetimepicker({
			language: 'ru',
			pickTime: false
		});
	}
	
	
	var date = new Date();
	date.setDate(date.getDate() - 1);

	if($('#deadline').length > 0){
		$('#deadline').datetimepicker({
			language: 'ru',
			pickTime: false,
			minDate: date
		});
	}
		
	$('.find_client').on('click',function(){
		$('#find_client').modal();
		return false;
	});

	$('.change_manager').on('click',function(){
		$('#change_manager').modal();
		return false;
	});

	$('.edit_phone, .edit_email').on('click',function(){
        //$('#code_true_modal').modal();
		var p = $(this).closest('.contact_controls');
		$(this).hide();
		$(p).find('.save_contact').css('display','inline-block');
		$(p).find('.cancel_contact').css('display','inline-block');
		$(this).closest('.line').find('input.form-control').removeAttr('disabled');
		return false;
	});
	
	$('.show_modal_report').on('click',function(){
		var id = $(this).attr('href');
		$(id).modal();
		return false;
	});

	$('#complain_manager').on('click',function(){
		$('#complain_manager_block').modal();
		return false;
	});

	$('#order_call_link2').on('click',function(){
		$('#order_call_block_modal').modal();
		return false;
	});
	
	$('.show_edit').on('click',function(){
		var form = $(this).closest('form');
		$(form).find('.files').slideDown();
		$('#file_list').show();
		$(this).hide();
		$('.close_data_edit').show();
		$(form).find('.show_edit_block').slideDown();
		$(form).find('.form-control').removeAttr('disabled');
		$(form).find('#like_fact').removeAttr('disabled');
		$(form).find('button.dropdown-toggle').removeClass('disabled');
		$(form).find('.bootstrap-select li').removeClass('disabled');
		
		if($('#like_fact:checked').length > 0) $('#post_address').attr('disabled','disabled');
		
		return false;
	});
	
	$('.show_edit_client').on('click',function(){
		var form = $('#personal_data');
		$(form).find('.files').slideDown();
		$('#file_list').show();
		$('.hide_during_edition').hide();
		$('.show_during_edition').show();
		$('.show_td_during_edition').css('display','table-cell');
		$('.show_inline_during_edition').css('display','inline');
		$(form).find('.form-control').removeAttr('disabled');
		$(form).find('#like_fact').removeAttr('disabled');
		$(form).find('button.dropdown-toggle').removeClass('disabled');
		$(form).find('.bootstrap-select li').removeClass('disabled');
		if($('#like_fact:checked').length > 0) $('#post_address').attr('disabled','disabled');
		return false;
	});
	
	$('.cancel_edit_client, .close_data_edit').on('click',function(){
		var form = $('#personal_data');
		$(form).find('.files').slideUp();
		$('#file_list').hide();
		$('.close_data_edit').hide();
		$('.show_edit').show();
		
		$('.hide_during_edition').show();
		$('.show_during_edition').hide();
		$('.show_td_during_edition').hide();
		$('.show_inline_during_edition').hide();
		$(form).find('.form-control').attr('disabled','disabled');
		$(form).find('#like_fact').attr('disabled','disabled');
		$(form).find('button.dropdown-toggle').attr('disabled','disabled');
		$(form).find('.bootstrap-select li').attr('disabled','disabled');
		return false;
	});
	
	$('.show_more').on('click',function(){
		var tr = $(this).closest('tr');
		$(this).hide();
		if(!$(tr).find('.name_item').is('.active')){
			$(tr).find('.name_item').addClass('active');
			var id = $(this).attr('id');
			$(tr).find('.hide_more').show();
			$('tr[rel='+id+']').css('display','table-row');
		}
		return false;
	});
	
	$('.hide_more').on('click',function(){
		var tr = $(this).closest('tr');
		$(tr).find('.show_more').show();
		$(tr).find('.name_item').removeClass('active');
		var id = $(tr).find('.show_more').attr('id');
		$(tr).find('.hide_more').hide();
		$('tr[rel='+id+']').css('display','none');
		return false;
	});
	
	//скрипт инициализации фенсибокса
	if($(".show_report").length > 0){
		$(".show_report").fancybox({
			beforeShow : function() {
				var title = this.title || '';
				this.title = (title && title.length ? title : '' );
			},
			helpers:  {
				title : {
					type : 'inside',
					position : 'top'
				},
				overlay : {
					showEarly : false
				},
				thumbs: {
					padding : 3,
					type : 'inside',
					width	: 10,
					height	: 10,
					posotion: 'bottom'
				}
			},
			nextClick : true,
			nextEffect :'fade',
			prevEffect : 'fade',
			padding : [15, 15, 15, 15],
			tpl: {
				closeBtn : '<div class="close"><a class="close_icon" href="#"></a></div>'
			},
			autoResize : false,
			autoSize : true,
			autoDimensions : false,
			fitToView: false
		});
	}
	
	$('.cancel').on('click',function(){
		var form = $(this).closest('form');
		$(form).find('.files').slideUp();
		$('#file_list').hide();
		$('.close_data_edit').hide();
		$('.show_edit').show();
		
		$(form).find('.show_edit_block').slideUp();
		$(form).find('#like_fact').attr('disabled','disabled');
		$(form).find('.form-control').attr('disabled','disabled');
		$(form).find('button.dropdown-toggle').addClass('disabled');
		$(form).find('.bootstrap-select li').addClass('disabled');
		return false;
	});
	
	
		
	$('#change_manager').on('hidden.bs.modal', function (e) {
		$('.change_manager_check').hide();
		$('.common_block').show();
		$('.fields_block').show();
		$('.form_success').hide();
		$('#change_manager_form').find('input, textarea').not('[type="hidden"], [type="button"], [type="submit"]').val('');
	});

	
		
	if ($('.personal_page').length > 0){
		$('.bufer_block').height($('.manager_block').height());
		
		$('#order_call_link').on('click',function(){
			var m = $('.manager_block');
			var new_h = m - 30;
			$('#order_call_block').height(new_h);
			$('.manager_block').fadeOut(500,function(){
				$('#order_call_block').fadeIn();
			});
			return false;
		});
		
		$('.close_link, .close_button').on('click',function(){
			$('#order_call_block').fadeOut(500,function(){
				$('.manager_block').fadeIn();
				
			});
			$('#order_call').find('.form_success').fadeOut(1000);
			$('#order_call').find('.fields_block').fadeIn(1000);
			return false;
		});
		
		//скрипт для валидации формы
		if ($('#order_call').length > 0) {
			$('#order_call').validate();
			action_form_test($('#order_call'));
			
		}
	}
	
	$('.edit_directories .edit').on('click',function(){
		var tr = $(this).closest('tr');
		$(tr).find('.edit').hide();
		$(tr).find('.delete').hide();
		$(tr).find('.save').show();
		$(tr).find('.cancel').show();
		$(tr).find('input.position_value').removeAttr('disabled');
		return false;
	});
	
	$('.edit_directories .cancel').on('click',function(){
		var tr = $(this).closest('tr');
		$(tr).find('.cancel').hide();
		$(tr).find('.save').hide();
		$(tr).find('.edit').show();
		$(tr).find('.delete').show();
		$(tr).find('input.position_value').attr('disabled','disabled');
		return false;
	});
	
	$('.edit_directories .save').on('click',function(){
		var tr = $(this).closest('tr');
		var type = $(this).closest('.edit_directories').attr('id');
		$(tr).find('form').attr('action','#edit_'+type).submit();
		console.log($(tr).find('form').attr('id'));
		$(tr).find('.cancel').hide();
		$(tr).find('.save').hide();
		$(tr).find('.edit').show();
		$(tr).find('.delete').show();
		$(tr).find('input.position_value').attr('disabled','disabled');
		return false;
	});
	
	$('.delete_bill').on('click',function(){
		$('#delete_modal2').modal();
		return false;
	});
	
	
	$('.edit_directories .delete').on('click',function(){
		var tr = $(this).closest('tr');
		var type = $(this).closest('.edit_directories').attr('id');
		$('#delete_modal form').find('.field_id').val($(tr).find('.position_id').val());
		$('#delete_modal form').find('.field_type').val(type);
		$('#delete_modal form').attr('action','#delete_'+type);
		$('#delete_modal').modal();
		return false;
	});
	
	$('#delete_modal form').submit(function(){
		var type = $('#delete_modal form').find('.field_type').val();
		var id = $('#delete_modal form').find('.field_id').val();
		$('#delete_modal').modal('hide');
		$('#'+type+'_'+id).closest('tr').remove();
		return false;
	});
	
	//скрипт для валидации формы
	if ($('.edit_directories').length > 0) {
		$('.edit_directories form').each(function(){
			var id = $(this).attr('id');
			$('#'+id).validate();
			action_form_test($('#'+id));
		});
	}
	
	//скрипт для валидации формы
	if ($('#complain_manager_form').length > 0) {
		$('#complain_manager_form').validate();
		action_form_test($('#complain_manager_form'));
		
	}
	
	if ($('#change_manager_form').length > 0) {
		$('#change_manager_form').validate();
		action_form_test($('#change_manager_form'));
		
	}
	
	//скрипт для валидации формы
	if ($('#block_remember_password').length > 0) {
		$('#block_remember_password').validate();
		action_form_remember($('#block_remember_password'));
		
	}
	
	
	//скрипт для валидации формы
	if ($('#sms_remember_password').length > 0) {
		$('#sms_remember_password').validate();
		action_form_remember_sms($('#sms_remember_password'));
		
	}
	
	//скрипт для валидации формы
	if ($('#login_form').length > 0) {
		$('#login_form').validate();
		//action_form_login_sms($('#login_form'));
		
	}
	
	
	//скрипт для валидации формы
	if ($('#change_manager').length > 0) {
		$('#change_manager').validate();
		action_form_test($('#change_manager'));
		
	}
	
	//скрипт для валидации формы
	if ($('#personal_manager_data').length > 0) {
		$('#personal_manager_data').validate();
		action_form_test($('#personal_manager_data'));
		
	}
	
	
	//скрипт для валидации формы
	if ($('#order_call2').length > 0) {
		$('#order_call2').validate();
		action_form_test($('#order_call2'));
		
	}
	
	//скрипт для валидации формы
	if ($('#password_change_block').length > 0) {
		/*$.validator.setDefaults({
			debug: true,
			success: "valid"
		});*/
		
		$('#password_change_block').validate({
			/*rules: {
				password: "required",
				password_again: {
					equalTo: "#password"
				}
			}	*/	
		});
		action_form_test_sms($('#password_change_block'));
		
	}
	
	$('.close_form_modal, .close_icon').on('click',function(){
		var form = $(this).closest('form');
		$(form).find('.fields_block').delay(1000).show();
		$(form).find('.form_loading').delay(1000).hide()
		$(form).find('.form_success').delay(1000).hide();
		//return false;
	});
	
	$('.modal_form .close_icon').on('click',function(){
		var form = $(this).closest('.modal-content').find('form');
		$(form).find('.fields_block').delay(1000).show();
		$(form).find('.form_loading').delay(1000).hide()
		$(form).find('.form_success').delay(1000).hide();
		//return false;
	});
	
	$('.change_manager_true').on('click',function(){
		$('.common_block').slideUp();
		$('.change_manager_check').slideDown();
		return false;
	});
});

// функция подгрузки лодера для формы
function form_loader(form){
	$(window).resize(function(){
		$(form).find('.form_loading').height($(form).find('.fields_block').height()+'px').width($(form).find('.fields_block').outerWidth()+'px');
	});
	$(form).find('.form_loading').height($(form).find('.fields_block').height()+'px').width($(form).find('.fields_block').outerWidth()+'px');
	$(form).find('.form_loading').fadeIn(500);
}

//функция обработки формы тестовая
	function action_form_test(form){
		//отправление формы
		$(form).submit(function(){
			if ($(form).find('input.error,textarea.error').length <= 0){
				form_loader(form);
				
				setTimeout(function(){
					success_form_test(form);
				}, 2000);
				
			}
			return false;
		});
	}
	
//функция обработки формы тестовая
	function action_form_remember(form){
		//отправление формы
		$(form).submit(function(){
			if ($(form).find('input.error,textarea.error').length <= 0){
				$('.block_remember_password').slideUp();
				$('.sms_remember_password').slideDown();
				
			}
			return false;
		});
	}
//функция обработки формы тестовая
	function action_form_remember_sms(form){
		//отправление формы
		$(form).submit(function(){
			if ($(form).find('input.error,textarea.error').length <= 0){
				$('.main_login').slideDown();
				$('.sms_remember_password').slideUp();
				
			}
			return false;
		});
	}
//функция обработки формы тестовая
	function action_form_login_sms(form){
		//отправление формы
		$(form).submit(function(){
			if ($(form).find('input.error,textarea.error').length <= 0){
				$('.main_login').slideUp();
				$('.sms_true_block').slideDown();
				
			}
			return false;
		});
	}
	
//функция обработки формы тестовая с sms
	function action_form_test_sms(form){
		//отправление формы
		$(form).submit(function(){
			if ($(form).find('input.error,textarea.error').length <= 0){
				form_loader(form);
				
				setTimeout(function(){
					success_form_test_sms(form);
				}, 2000);
				
			}
			return false;
		});
	}
	
//функция обработки формы
function action_form(form){
	//отправление формы
	$(form).submit(function(){
		if ($(form).find('input.error,textarea.error').length <= 0){

			$(form).ajaxSubmit({
				beforeSubmit : function(){
					form_loader(form);
				},
				success: function(data) {
					var ok = true;
					$(data).find("#system-errors li").each(function(){
						ok = false;
					});

					if(ok){
						success_form(form);
					}
				}
			});
		}
		return false;
	});
}

// функция успеха для формы
function success_form(form){
	$(form).find('.fields_block').fadeOut(500);
	$(form).find('.form_loading').fadeOut(500, function(){
		$(form).find('.form_success').fadeIn(500);
	});
	if ($(form).attr('id') == "comment_form"){
		setTimeout(function(){
			$(form).find('.close_form').click();
			$(form).find('.form_success').fadeOut(1000);
			$(form).find('.fields_block').fadeIn(1000);
		}, 2000);
	}				
	$(form).find('input, textarea').not('[type="hidden"], [type="button"], [type="submit"]').val('');
}

// функция успеха для формы 2
function success_form_test(form){
	$(form).find('.fields_block').fadeOut(500);
	$(form).find('.form_loading').fadeOut(500, function(){
		$(form).find('.form_success').fadeIn(500);
	});
	//$(form).find('input, textarea').not('[type="hidden"], [type="button"], [type="submit"]').val('');
}

// функция успеха для формы 2 с sms
function success_form_test_sms(form){
	$(form).find('.form_loading').fadeOut(500);
	/*$(form).find('.form_loading').fadeOut(500, function(){
		$(form).find('.form_success').fadeIn(500);
	});
	$(form).find('input, textarea').not('[type="hidden"], [type="button"], [type="submit"]').val('');*/
	$('#code_true_modal').modal();
}