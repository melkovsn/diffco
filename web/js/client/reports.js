$(document).ready(function () {

    var timer;
    var $confirmCode = $('#confirm_code');
    var $confirmCodeTime = $('#confirm_code_time');
    var $codeTrueModal = $('#code_true_modal');

    $('#reports_search').submit(function (event) {

        event.preventDefault();

        var form = document.forms.reports_search;

        var formData = new FormData(form);
        var xhr = new XMLHttpRequest();

        xhr.open("POST", "/+/client/searchReports");

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    data = xhr.responseText;

                    $('#reports_search_result').html(data);

                    $('.subscribe').on('click', function () {

                        clearInterval(timer);
                        $confirmCodeTime.val('');
                        $confirmCode.val('');
                        $confirmCode.removeClass('error');

                        $.post("/+/user/sendConfirmCode", function (data) {
                            data = jQuery.parseJSON(data);
                            if (data['status'] == 'success') {
                                $codeTrueModal.modal();

                                countdown(data['time_alive']);
                            }
                        });
                        
                        $codeTrueModal.attr('data_id', ($(this).attr('data_id')));

                        return false;
                    });

                    function countdown(seconds) {

                        timer = setInterval(function () {

                            var date = new Date(1970, 0, 1);

                            date.setSeconds(seconds--);

                            if (seconds < 0) {
                                clearInterval(timer)
                            }

                            var timeAlive = moment(date);
                            $confirmCodeTime.val(timeAlive.format('mm:ss'));

                        }, 1000);
                    }
                }
            }
        };

        xhr.send(formData);
    });
    
    $('#confirm_code_form').submit(function (event) {

        event.preventDefault();

        var reportId = $codeTrueModal.attr('data_id');

        var data = {'reportId': reportId, 'confirm_code': $confirmCode.val()};

        $.post("/+/client/signReport", data, function (data) {
            data = jQuery.parseJSON(data);

            if (data['status'] == 'success') {
                
                $('.subscribe[data_id=' + reportId + ']').parent('td').html('Подписан');

                $codeTrueModal.modal('hide');
            } else if (data['status'] == 'code_error') {
                if (!$confirmCode.hasClass('error')) {
                    $confirmCode.addClass('error');
                }
            } else {
                //$field.addClass('error');
                $codeTrueModal.modal('hide');
            }

        });

    });


    $('#reports_search').submit();

    $('#reports_search .period_control').on('click', function () {

        var data = $(this).data();

        $('#start_date').val(data['start_date']);
        $('#end_date').val(data['end_date']);

        $('#reports_search').submit();

        return false;
    });

    if ($('#start_date').length > 0) {
        $('#start_date').datetimepicker({
            language: 'ru',
            pickTime: false
        });
    }

    if ($('#end_date').length > 0) {
        $('#end_date').datetimepicker({
            language: 'ru',
            pickTime: false
        });
    }

});