<div class="row bank_block">
    <div class="col-md-6">
        <div class="inner-block">
            <div class="inner-top">
                <h3>Банковские реквизиты</h3>
                <a href="#" class="bank_block_edit_link" data_bank-details-id="0" >Добавить</a>
            </div>
            <div class="padding">
                <div class="table_block">
                    <table>
                        <thead>
                            <tr>
                                <td>Название</td>
                                <td>Валюта</td>
                                <td class="with_button">Подпись</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($bankDetails as $bankDetail) : ?>
                                <tr>
                                    <td><?= $bankDetail->getNameAccount(); ?></td>
                                    <td><?= $bankDetail->getCurrency()->getSign(); ?></td>
                                    <td class="with_button">
                                        <a href="#" class="linkButton2 bank_block_edit_link" data_bank-details-id="<?= $bankDetail->getId(); ?>">Изменить</a></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="inner-block bank_block_edit">
    <div class="row">
        <form action="#" id="bank_block_edit_form">
            <div class="col-md-12">
                <div class="inner-top">
                    <h3>Реквизиты банковского счета</h3>
                    <a href="#" class="close_bank_edit close_icon"></a>
                </div>
                <div class="clearfix visible-sm"></div>
                
                <div class="fields_paddings" id="bank_block_edit_form_fields">
                
                </div>

            </div>
        </form>
    </div>

</div>