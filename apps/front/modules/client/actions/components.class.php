<?php

/**
 * Клиент components
 * 
 * No redirection nor database manipulation ( insert, update, delete ) here
 * 
 */
class clientComponents extends myFrontModuleComponents {

    public function executePassportData(dmWebRequest $request) {

        $this->client = DfAuthUser::getInstance()->getAuthUser();
        
        $this->form = new ClientPassportDataFeedbackForm($this->client);     
    }

    public function executeInformData(dmWebRequest $request) {
        $this->client = DfAuthUser::getInstance()->getAuthUser();
    }

    public function executeBankDetails(dmWebRequest $request) {
        $this->client = DfAuthUser::getInstance()->getAuthUser();
        
        $this->bankDetails = $this->client->getBankDetails();
    }

    public function executeClientData(dmWebRequest $request) {
        // Your code here
    }

}
