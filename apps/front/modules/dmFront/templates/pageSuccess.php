<div class="page-wrapper <?php $isEditMode && print ' edit' ?>" id="dm_page">
	<div class="main_container dm_layout">

		<div class="navbar-wrapper">

			<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div class="navbar-header">
					<div class="container">
						<div class="row header_line">
							<?php include_partial('main/header/logo');?>
						</div>
					</div>		
				</div>
			</div>

		</div>
        
        <?//php include_component('user', 'auth')?>    

		<div class="clear-footer"></div>
	</div>
</div>
<?php echo $helper->renderArea('page.content') ?>

<div id="footer">

	<!-- FOOTER -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-4">© 2010-2014 <a href="#" target="_blank">Finance Russia</a></div>
				<div class="col-md-4 center">Офис: +7 (495) 111-11-11</div>
				<div class="col-md-4">
					<div class="drucom">
						<div class="drucom-info">
							Разработка проекта<br/><a href="http://drucom.ru/" target="_blank">Другая Компания</a>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</footer>		
</div>
