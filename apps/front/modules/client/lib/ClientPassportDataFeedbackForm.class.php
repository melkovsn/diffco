<?php

/**
 * Client form.
 *
 * @package    diffco
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id$
 * @generator  Diem 5.4.0-DEV
 * @gen-file   /home/user/projects/diffco/lib/vendor/diem/dmCorePlugin/data/generator/dmDoctrineForm/default/template/sfDoctrineFormTemplate.php */
class ClientPassportDataFeedbackForm extends BaseClientForm {

    public function configure() {

        parent::configure();

        $this->disableLocalCSRFProtection();

        $this->setWidgets(array(
            'name' => new sfWidgetFormInputText(array(), array('class' => 'form-control required', 'disabled' => 'disabled')),
            'surname' => new sfWidgetFormInputText(array(), array('class' => 'form-control required', 'disabled' => 'disabled')),
            'patronymic' => new sfWidgetFormInputText(array(), array('class' => 'form-control required', 'disabled' => 'disabled')),
            'birth_date' => new sfWidgetFormInputText(array(), array('class' => 'date form-control required', 'disabled' => 'disabled', 'data-date-format' => 'DD.MM.YYYY')),
            'birth_place' => new sfWidgetFormInputText(array(), array('class' => 'form-control required', 'disabled' => 'disabled')),
            'post_address' => new sfWidgetFormInputText(array(), array('class' => 'form-control required', 'disabled' => 'disabled')),
            'passport_series' => new sfWidgetFormInputText(array(), array('class' => 'form-control required', 'disabled' => 'disabled')),
            'passport_number' => new sfWidgetFormInputText(array(), array('class' => 'form-control required', 'disabled' => 'disabled')),
            'registration' => new sfWidgetFormInputText(array(), array('class' => 'form-control required', 'disabled' => 'disabled')),
            'passport_date' => new sfWidgetFormInputText(array(), array('class' => 'date form-control required', 'disabled' => 'disabled', 'data-date-format' => 'DD.MM.YYYY')),
            'inn' => new sfWidgetFormInputText(array(), array('class' => 'form-control required', 'disabled' => 'disabled')),
            'place_of_issue' => new sfWidgetFormInputText(array(), array('class' => 'form-control required', 'disabled' => 'disabled', 'placeholder' => 'кем')),
            'passport_scan' => new sfWidgetFormInputFile(array(), array('class' => 'custom_file')),
        ));

        $countryChoices = CountryTable::getInstance()->getNamesOfCountries();

        $this->setWidget('country_id', new sfWidgetFormChoice(array('choices' => $countryChoices, 'multiple' => false, 'expanded' => false), array('class' => 'form-control', 'disabled' => 'disabled')));
        $this->setValidator('country_id', new sfValidatorChoice(array('choices' => array_keys($countryChoices), 'multiple' => false, 'required' => true)));

        $residentChoises = array(0 => 'нерезидент', 1 => 'резидент');

        $this->setWidget('is_resident', new sfWidgetFormChoice(array('choices' => $residentChoises, 'multiple' => false, 'expanded' => false), array('class' => 'form-control', 'disabled' => 'disabled')));
        $this->setValidator('is_resident', new sfValidatorBoolean(array('required' => false)));

        $this->setValidator('passport_scan', new sfValidatorFile(array("required" => false)));

        unset($this->validatorSchema['phone_sms_info']);
        unset($this->validatorSchema['email']);
        unset($this->validatorSchema['company']);
        unset($this->validatorSchema['login']);
        unset($this->validatorSchema['dm_user_id']);
    }

    public function sendMailForManager() {

        $client = DfAuthUser::getInstance()->getAuthUser();
        $manager = $client->getManager();
        
        $aMailData = array(
            'surname' => (string) $this->getValue('surname'),
            'name' => (string) $this->getValue('name'),
            'patronymic' => (string) $this->getValue('patronymic'),
            'birth_date' => (string) $this->getValue('birth_date'),
            'birth_place' => (string) $this->getValue('birth_place'),
            'post_address' => (string) $this->getValue('post_address'),
            'passport_series' => (string) $this->getValue('passport_series'),
            'passport_number' => (string) $this->getValue('passport_number'),
            'registration' => (string) $this->getValue('registration'),
            'passport_date' => (string) $this->getValue('passport_date'),
            'inn' => (string) $this->getValue('inn'),
            'place_of_issue' => (string) $this->getValue('place_of_issue'),
            'client_id' => $client->getId(),
            'mailto' => (string) $manager->getEmail()
        );

        $mail = $this->getService('mail')
                ->setTemplate('client_passport_data_change_for_manager')
                ->addValues($aMailData)
                ->render();

        if ($this->getValue('passport_scan') instanceof sfValidatedFile) {
            $file = $this->getFile($this->getValue('passport_scan'));
            $mail->getMessage()->attach(Swift_Attachment::fromPath($file));
        }

        $mail->send();

        if (isset($file)){
            $this->removeFile($file);
        }
    }

    protected function getFile($oFile) {
        if (!$oFile) {
            return null;
        }
        $sPath = sfConfig::get('sf_upload_dir') . '/scans';
        if (!file_exists($sPath)) {
            mkdir($sPath);
        }
        $sDirName = sha1($oFile->getOriginalName() . rand(11111, 99999));
        $sPath = $sPath . '/' . $sDirName;
        mkdir($sPath);
        $sFile = $sPath . '/' . $oFile->getOriginalName();
        $oFile->save($sFile);

        return $sFile;
    }

    protected function removeFile($sFile) {
        unlink($sFile);
        rmdir(dirname($sFile));
    }

}
