<?php

class DfLoginForm extends sfForm {

    protected $sBackLink = '';

    public function configure() {

        $this->disableLocalCSRFProtection();

        $aDefaults = $this->getDefaults();

        $this->setWidgets(array(
            'login' => new sfWidgetFormInputText(),
            'password' => new sfWidgetFormInputPassword(),
        ));

        $this->setValidators(array(
            'login' => new sfValidatorString(),
            'password' => new sfValidatorString()
        ));

        $this->validatorSchema->setPostValidator(
                new sfValidatorCallback(array('callback' => array($this, 'validateUser')))
        );
    }

    public function validateUser($validator, $values) {
        
        $userId = DfAuthUser::getInstance()->findUser($values['login'], $values['password']);
            
        if (!$userId) {
            throw new sfValidatorError($this->validatorSchema['login'], 'error');
        }
        return $values;
    }

}