<?php

/**
 * Отчёт components
 * 
 * No redirection nor database manipulation ( insert, update, delete ) here
 */
class reportComponents extends myFrontModuleComponents {

    public function executeReportListClient(dmWebRequest $request) {
        
        $client = DfAuthUser::getInstance()->getAuthUser();
        
        $this->startDate = date("d.m.Y", strtotime("-3 months"));
        $this->endDate = date("d.m.Y");
                
        $this->reports = $client->getReportsByPeriod($this->startDate, $this->endDate, true);
        
    }

    public function executeReportDetail(dmWebRequest $request) {
        // Your code here
    }

}
